<?php

$temperatures = array(78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62,
 62, 65, 64, 68, 73, 75, 79, 73);

$suma = 0;
$cantidad = count($temperatures);

foreach($temperatures as $key){
    $suma += $key;
}
$promedio = $suma/$cantidad;
echo "Temperatura Promedio: $promedio".PHP_EOL;

//MINIMO

$minimo = $temperatures;
$dis =array_unique($minimo);
asort($dis);

$res= array_slice($dis, 0, 5);
$string="";
foreach ($res as $key) {
    $string=$string.$key." ";
}
echo "Las 5 temperaturas más bajas: $string".PHP_EOL;

//MAXIMO

$maximo = $temperatures;
$dis =array_unique($maximo);
arsort($dis);

$res= array_slice($dis, 0, 5);
$string="";
foreach ($res as $key) {
    $string=$string.$key." ";
}
echo "Las 5 temperaturas más altas: $string".PHP_EOL;