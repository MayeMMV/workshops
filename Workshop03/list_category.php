<?php
  // get all users from the database
  $sql = 'SELECT * FROM categories';
  $connection = new mysqli('localhost', 'root', '', 'crud');
  $result = $connection->query($sql);
  $categories = $result->fetch_all();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  <title>Document</title>
</head>
<body>
<div class="container">
  <?php require ('header.php') ?>
  <h1>List of Categories</h1>
    <table class="table table-light">
      <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Actions</th>
      </tr>
      <tbody>
        <?php
          // loop categories
          foreach($categories as $category) {
            echo "<tr><td>".$category[0]."</td><td>".$category[1]."</td><td><a href=\"edit.php?id="."\">Edit</a> | <button type=\"button"."\" class=\"btn btn-secondary"."\" onclick=\"delete.php"."\">Delete</button></td></tr>";
          }
        ?>
      </tbody>
    </table>
    <?php

  $connection->close();
?>
</div>
</body>
</html>